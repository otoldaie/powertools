#!/bin/bash

#
# Enable the power on a X hybrid
#

# Set ID, this might fail if ID is already set
./bin/endeavour setid idpads 16

#
# Enable the DCDC
./bin/endeavour write 42 1
./bin/endeavour write 43 1

#
# Enable the Hybrid

# resetB
./bin/endeavour write 46 0x000100
./bin/endeavour write 47 0x000100

# Hybrid LDOs
./bin/endeavour write 41 0x66660000
./bin/endeavour write 40 0x66660000

