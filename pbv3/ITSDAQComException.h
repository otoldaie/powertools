#ifndef ITSDAQCOMEXCEPTION_H
#define ITSDAQCOMEXCEPTION_H

#include <stdarg.h>

#include <string>

#include "ComException.h"

class ITSDAQComException : ComException
{
public:
  ITSDAQComException(const std::string& msg);
  ITSDAQComException(const char *format, ...);

  virtual const char* what() const throw();

private:
  std::string m_msg;
};

#endif // ITSDAQCOMEXCEPTION_H
