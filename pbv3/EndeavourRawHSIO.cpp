#include "EndeavourRawHSIO.h"

#include "EndeavourComException.h"
#include "NotSupportedException.h"

#include <cstdint>
#include <iostream>
#include <iomanip>
#include <bitset>
#include <unistd.h>
#include <stdint.h>
#include <arpa/inet.h>

// More info on the ITSDAQ op-codes
// https://twiki.cern.ch/twiki/bin/view/Atlas/ITSDAQDataFormat#EMORSE

#define CMD_SETID 0b110
#define CMD_WRITE 0b111
#define CMD_READ  0b101

EndeavourRawHSIO::EndeavourRawHSIO()
{
  const char* params[3]={"/tmp/hsioPipe.fromHsio","/tmp/hsioPipe.toHsio","\0"};
  m_access.reset(hsio_init("file",params));
}

EndeavourRawHSIO::~EndeavourRawHSIO()
{ }

void EndeavourRawHSIO::setDitMin(uint32_t DIT_MIN)
{ throw NotSupportedException("Variable length dits not supported."); }

uint32_t EndeavourRawHSIO::getDitMin()
{ throw NotSupportedException("Variable length dits not supported."); }

void EndeavourRawHSIO::setDitMid(uint32_t DIT_MID)
{ throw NotSupportedException("Variable length dits not supported."); }

uint32_t EndeavourRawHSIO::getDitMid()
{ throw NotSupportedException("Variable length dits not supported."); }

void EndeavourRawHSIO::setDitMax(uint32_t DIT_MAX)
{ throw NotSupportedException("Variable length dits not supported."); }

uint32_t EndeavourRawHSIO::getDitMax()
{ throw NotSupportedException("Variable length dits not supported."); }

void EndeavourRawHSIO::setDahMin(uint32_t DAH_MIN)
{ throw NotSupportedException("Variable length dahs not supported."); }

uint32_t EndeavourRawHSIO::getDahMin()
{ throw NotSupportedException("Variable length dahs not supported."); }

void EndeavourRawHSIO::setDahMid(uint32_t DAH_MID)
{ throw NotSupportedException("Variable length dahs not supported."); }

uint32_t EndeavourRawHSIO::getDahMid()
{ throw NotSupportedException("Variable length dahs not supported."); }

void EndeavourRawHSIO::setDahMax(uint32_t DAH_MAX)
{ throw NotSupportedException("Variable length dahs not supported."); }

uint32_t EndeavourRawHSIO::getDahMax()
{ throw NotSupportedException("Variable length dahs not supported."); }

void EndeavourRawHSIO::setBitGapMin(uint32_t BITGAP_MIN)
{ throw NotSupportedException("Variable length bit gap not supported."); }

uint32_t EndeavourRawHSIO::getBitGapMin()
{ throw NotSupportedException("Variable length bit gap not supported."); }

void EndeavourRawHSIO::setBitGapMid(uint32_t BITGAP_MID)
{ throw NotSupportedException("Variable length bit gap not supported."); }

uint32_t EndeavourRawHSIO::getBitGapMid()
{ throw NotSupportedException("Variable length bit gap not supported."); }

void EndeavourRawHSIO::setBitGapMax(uint32_t BITGAP_MAX)
{ throw NotSupportedException("Variable length bit gap not supported."); }

uint32_t EndeavourRawHSIO::getBitGapMax()
{ throw NotSupportedException("Variable length bit gap not supported."); }

void EndeavourRawHSIO::reset()
{  }

bool EndeavourRawHSIO::isError()
{ return m_error; }

bool EndeavourRawHSIO::isDataValid()
{ return m_valid; }

void EndeavourRawHSIO::sendData(unsigned long long int data, unsigned int size)
{
  m_valid=false;
  m_error=false;

  // Determine command type (◔_◔)
  uint8_t cmd=(data>>(size-3))&0x7;
  if(cmd!=CMD_SETID && cmd!=CMD_WRITE && cmd!=CMD_READ)
    return;

  //
  // Send data
  std::vector<uint16_t> inBuffer(10);

  // header
  uint16_t chan=1; // TODO unhardcode
  inBuffer[0]=0x0000;     //version
  inBuffer[1]=chan & 0xf; //control
  inBuffer[2]=0x0000;     //spare

  // packet data
  uint64_t packet=data|(((uint64_t)(cmd==CMD_SETID))<<60)|(((uint64_t)(cmd==CMD_READ))<<56);
  inBuffer[3]=((packet>>48)&0xFFFF);
  inBuffer[4]=((packet>>32)&0xFFFF);
  inBuffer[5]=((packet>>16)&0xFFFF);
  inBuffer[6]=((packet>> 0)&0xFFFF);

  hsio_send_single_opcode(m_access.get(), HSIO_MORSE_OPCODE, ++m_seqnum,
                          inBuffer.size(), &inBuffer[0]);

  //
  // Await correct response (or timeout)
  std::unique_ptr<HsioOpcode> opHandle;
  opHandle = hsio_receive_buffered_packet_with_timeout(m_access.get(), HSIO_MORSE_OPCODE, 2000);

  if(!opHandle)
    throw EndeavourComException("HSIO timeout waiting for packet with seqnum " + std::to_string(m_seqnum));

  // Check sequence number
  uint16_t got = opHandle->seqnum();
  if(m_seqnum != got)
    throw EndeavourComException("HSIO rejecting packet with wrong seqnum exp " + std::to_string(m_seqnum) + " got " + std::to_string(got));

  // Get the response data
  uint16_t  outLength = 0;
  uint16_t* outBuffer = hsio_opcode_extract_data(&*opHandle, &outLength);

  if(outLength == 0)
    throw EndeavourComException("HSIO returned no data");

  std::vector<uint16_t> returnBuffer(outLength);
  for (int w = 0; w<outLength; w++)
    returnBuffer[w] = ntohs(outBuffer[w]);

  uint16_t  actualBus = returnBuffer[1];
  if (actualBus != inBuffer[1])
    throw EndeavourComException("HSIO rejecting packet from bus " + actualBus);

  // //
  // // Extract the response
  // for(uint16_t x : returnBuffer)
  //   std::cout << std::hex << std::setw(4) << std::setfill('0') << (uint32_t)x << std::dec << std::endl;

  //
  // Reconstruct the AMAC response
  uint64_t code=(returnBuffer[4]>>12)&0x0F; //timeout or error
  uint64_t amac=(returnBuffer[4]>>11)&0x1F;
  uint64_t addr=(returnBuffer[4]>> 0)&0xFF;
  uint64_t regv=(((uint64_t)returnBuffer[5])<<16 | returnBuffer[6]);

  if(code==0xF) // Timeout!
    {
      m_valid=false;
      return;
    }

  switch(cmd)
    {
    case CMD_SETID:
      {
	uint64_t retid=(returnBuffer[6]>>3)&0x1F;
	m_readData=(retid<<3)|(((uint64_t)0)<<0);
	m_readSize=8;
	m_valid=true;
	m_error=false;
      }
      break;
    case CMD_WRITE:
      {
	// ITSDAQ firmware does not return AMAC ID
	amac=(((data>>8)>>32)>>8)&0x1F;
	m_readData=(amac<<3)|(((uint64_t)0)<<0);
	m_readSize=8;
	m_valid=true;
	m_error=false;
      }
      break;
    case CMD_READ:
      {
	m_readData=(amac<<43)|(((uint64_t)0)<<40)|(addr<<32)|(regv<< 0);
	m_readSize=48;
	m_valid=true;
	m_error=false;
      }
      break;
    default:
      {
	m_valid=false;
	m_error=true;
      }
      break;
    }
}

void EndeavourRawHSIO::readData(unsigned long long int& data, unsigned int& size)
{
  data=m_readData;
  size=m_readSize;
}
